<?php

require 'smarty.php';
require 'config_BD.php';

$acc = strlen(filter_input(INPUT_POST, 'accion')) ? filter_input(INPUT_POST, 'accion') : '';
$nom = strlen(filter_input(INPUT_POST, 'nombre')) ? filter_input(INPUT_POST, 'nombre') : '';
$ape = strlen(filter_input(INPUT_POST, 'apellido')) ? filter_input(INPUT_POST, 'apellido') : '';
$mai = strlen(filter_input(INPUT_POST, 'mail')) ? filter_input(INPUT_POST, 'mail') : '';
$pas = strlen(filter_input(INPUT_POST, 'pass')) ? filter_input(INPUT_POST, 'pass') : '';

if ($acc == "register") {
    if ($nom != '' && $ape != '' && mailValido($mai,$conn)) {
        if (passValida($pas)) {
            $sql = "insert into Usuarios (Nombre,Mail,Password) values (:nom, :mail, :pass)";

            $nombreCompleto = utf8_encode($nom . " " . $ape);
            $parametros = array();
            $parametros[] = array("nom", $nombreCompleto, "string");
            $parametros[] = array("mail", $mai, "string");
            $parametros[] = array("pass", $pas, "string");

            $conn->conectar();
            if ($conn->consulta($sql, $parametros)) {
                header("Location: index.php?accion=login&user=$mai&pass=$pas");         
                die();
            } else {
                die();
            }           
            $conn->desconectar();
            
        } else {
            $error = "La clave debe al menos 6 caracteres,"."<br>"."letra mayuscula,minuscula,numero y signo de puntuacion";
        }
    } else {
        $error = "Nombre, Apellido deben ser completados y el mail debe ser unico";
    }
}

function passValida($clave) {
    $clave_chrs = str_split($clave);
    $contiene_num = false;
    $contiene_punts = false;
    $cant = 0;
    if (!ctype_upper($clave) && !ctype_lower($clave)) {//Tengo caracteres en mayuscula y en minuscula
        foreach ($clave_chrs as $cTemp) {
            $cant++;
            if (ctype_digit($cTemp)) {
                $contiene_num = true;
            }
            if (ctype_punct($cTemp)) {
                $contiene_punts = true;
            }
        }
    }
    return $contiene_num && $contiene_punts && $cant>5;
}

function mailValido($unM,$bd) {
    $sql = "select * from Usuarios where Mail = :m";
    $parametros = array();
    $parametros[] = array("m", $unM, "string");
    
    $bd->conectar();
    if ($bd->consulta($sql, $parametros) && is_array($bd->siguienteRegistro())) { //mail no unico
       return false;
    }else{
        if(strrpos($unM, '@') < strrpos($unM, '.') && strrpos($unM, '@') > 1) {//tiene '@' antes de '.' y al menos 2 caracteres antes de '@'
            return true;
        }
    }
    $bd->desconectar();
    return false;
}
if(strlen($error)){
    $smarty->assign("error", $error);
}

$smarty->display("registro.tpl");
