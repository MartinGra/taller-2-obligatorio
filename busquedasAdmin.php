<?php

usleep(350000);

include_once("config_BD.php");

$accion = strlen(filter_input(INPUT_POST, 'accion')) ? filter_input(INPUT_POST, 'accion') : filter_input(INPUT_GET, 'accion');

$song = strlen(filter_input(INPUT_POST, 'cancion')) ? filter_input(INPUT_POST, 'cancion') : '';
$album = strlen(filter_input(INPUT_POST, 'album')) ? filter_input(INPUT_POST, 'album') : '';
$artist = strlen(filter_input(INPUT_POST, 'artista')) ? filter_input(INPUT_POST, 'artista') : '';

$cantElementosPorPagina = 10;
$p = strlen(filter_input(INPUT_POST, 'p')) ? filter_input(INPUT_POST, 'p') : 0;

$resultado = "";

if ($accion == 'listarTemas') {
    $cantidad = 0;
    $sql = "select count(*) as cantidad from Canciones where lower(Nombre) like lower('%$song%')";

    $conn->conectar();

    if ($conn->consulta($sql)) {
        $result = $conn->siguienteRegistro();
        $cantidad = $result["cantidad"];
    }

    $sql2 = "select * from Canciones where lower(Nombre) like lower('%$song%') order by Nombre asc limit :offset, :cantidad";

    $parametros = array();
    $parametros[] = array("offset", $p * $cantElementosPorPagina, "int");
    $parametros[] = array("cantidad", $cantElementosPorPagina, "int");

    if ($conn->consulta($sql2, $parametros)) {
        $result = $conn->restantesRegistros();
        $nombreAlbumes = array();
        $nombreArtistas = array();
        //Por cada Cancion busco el nombre de Album y por cada Album su Artista
        for($i = 0; $i < count($result); $i++){
            $idAlbTemp = $result[$i]['AlbumId'];
            $buscarAlb = "select * from Albumes where Id = $idAlbTemp";
            if($conn->consulta($buscarAlb)){
                $encontrado = $conn->siguienteRegistro();
                //Agrego nombre a la lista de los albumes
                $nombreAlbumes[] = $encontrado['Nombre'];
                
                $idArtTemp = $encontrado['IdArtista'];
                $buscarArt = "select Nombre from Artistas where Id = $idArtTemp";
                if($conn->consulta($buscarArt)){
                    //Agrego nombre a la lista de los artistas
                    $nombreArtistas[] = $conn->siguienteRegistro()['Nombre'];
                }  
            }
        }
        $arr = array(
            "cantidad" => ceil($cantidad / $cantElementosPorPagina),
            "pagina" => $p,
            "canciones" => $result,
            "nombreAlbumes" => $nombreAlbumes,
            "nombreArtistas" => $nombreArtistas
        );
        $resultado = json_encode($arr);
    } else {
        $resultado = $conn->ultimoError();
    }
    $conn->desconectar();
} else if ($accion == "listarAlbumes") {
    $cantidad = 0;
    $sql = "select count(*) as cantidad from Albumes where lower(Nombre) like lower('%$album%')";

    $conn->conectar();

    if ($conn->consulta($sql)) {
        $result = $conn->siguienteRegistro();
        $cantidad = $result["cantidad"];
    }

    $sql2 = "select * from Albumes where lower(Nombre) like lower('%$album%') order by Nombre asc limit :offset, :cantidad";

    $parametros = array();
    $parametros[] = array("offset", $p * $cantElementosPorPagina, "int");
    $parametros[] = array("cantidad", $cantElementosPorPagina, "int");

    if ($conn->consulta($sql2, $parametros)) {
        $result = $conn->restantesRegistros();
        $nombreArtistas = array();
        for($i = 0 ; $i < count($result) ;$i++){
            $idArtTemp = $result[$i]['IdArtista'];
            $buscarIdArt = "select Nombre from Artistas where Id = $idArtTemp";
            if($conn->consulta($buscarIdArt)){
                $nombreArtistas[] = $conn->siguienteRegistro()['Nombre'];
            }
        }
        
        $arr = array(
            "cantidad" => ceil($cantidad / $cantElementosPorPagina),
            "pagina" => $p,
            "albumes" => $result,
            "nombreArtistas" => $nombreArtistas
        );

        $resultado = json_encode($arr);
    } else {
        $resultado = $conn->ultimoError();
    }
    $conn->desconectar();
} else if ($accion == "listarArtistas") {
    $cantidad = 0;
    $sql = "select count(*) as cantidad from Artistas where lower(Nombre) like lower('%$artist%')";

    $conn->conectar();

    if ($conn->consulta($sql)) {
        $result = $conn->siguienteRegistro();
        $cantidad = $result["cantidad"];
    }

    $sql2 = "select * from Artistas where lower(Nombre) like lower('%$artist%') order by Nombre asc limit :offset, :cantidad";

    $parametros = array();
    $parametros[] = array("offset", $p * $cantElementosPorPagina, "int");
    $parametros[] = array("cantidad", $cantElementosPorPagina, "int");

    if ($conn->consulta($sql2, $parametros)) {
        $result = $conn->restantesRegistros();

        $arr = array(
            "cantidad" => ceil($cantidad / $cantElementosPorPagina),
            "pagina" => $p,
            "artistas" => $result
        );

        $resultado = json_encode($arr);
    } else {
        $resultado = $conn->ultimoError();
    }
    $conn->desconectar();
} else if ($accion == "listarUsuarios") {
    $cantidad = 0;
    $sql = "select count(*) as cantidad from Usuarios";

    $conn->conectar();

    if ($conn->consulta($sql)) {
        $result = $conn->siguienteRegistro();
        $cantidad = $result["cantidad"];
    }

    $sql2 = "select * from Usuarios order by Nombre asc limit :offset, :cantidad";

    $parametros = array();
    $parametros[] = array("offset", $p * $cantElementosPorPagina, "int");
    $parametros[] = array("cantidad", $cantElementosPorPagina, "int");

    if ($conn->consulta($sql2, $parametros)) {
        $result = $conn->restantesRegistros();

        $arr = array(
            "cantidad" => ceil($cantidad / $cantElementosPorPagina),
            "pagina" => $p,
            "usuarios" => $result
        );

        $resultado = json_encode($arr);
    } else {
        $resultado = $conn->ultimoError();
    }
    $conn->desconectar();
} else if ($accion == "listarReproducciones") {
    $cantidad = 0;
    $sql = "select count(*) as cantidad from Artistas";

    $conn->conectar();

    if ($conn->consulta($sql)) {
        $result = $conn->siguienteRegistro();
        $cantidad = $result["cantidad"];
    }

    $sql2 = "select a.Nombre, a.Pais , COALESCE(sum(c.Reproducciones),0) as Reproducciones
            from Artistas a
            left join Albumes b on b.IdArtista = a.Id
            left join Canciones c on c.AlbumId = b.Id
            group by a.Id,a.Nombre order by Reproducciones desc limit :offset, :cantidad";

    $parametros = array();
    $parametros[] = array("offset", $p * $cantElementosPorPagina, "int");
    $parametros[] = array("cantidad", $cantElementosPorPagina, "int");

    if ($conn->consulta($sql2, $parametros)) {
        $result = $conn->restantesRegistros();

        $arr = array(
            "cantidad" => ceil($cantidad / $cantElementosPorPagina),
            "pagina" => $p,
            "reproducciones" => $result
        );

        $resultado = json_encode($arr);
    } else {
        $resultado = $conn->ultimoError();
    }
    $conn->desconectar();
} else if ($accion == "borrarTema") {
    
    $idTema = strlen(filter_input(INPUT_POST, 'temaABorrar')) ? filter_input(INPUT_POST, 'temaABorrar') : '';
    $conn->conectar();
    $seBorro = deleteSongId($idTema, $conn);
    $resultado = ($seBorro) ? json_encode("Se ha eliminado correctamente") : "Problemas al borrar";
    $conn->desconectar();
    
} else if ($accion == "borrarAlbum") {

    $idAlb = strlen(filter_input(INPUT_POST, 'albumABorrar')) ? filter_input(INPUT_POST, 'albumABorrar') : '';
    $conn->conectar();
    $seBorro = deleteAlbumId($idAlb, $conn);
    $resultado = ($seBorro) ? json_encode("Se ha eliminado correctamente") : "Problemas al borrar";
    $conn->desconectar();
    
} else if ($accion == "borrarArtista") {
    $idArt = strlen(filter_input(INPUT_POST, 'artistaABorrar')) ? filter_input(INPUT_POST, 'artistaABorrar') : '';
    $conn->conectar();
    $borrar = "delete from Artistas where Id = $idArt";
    if ($conn->consulta($borrar)) {
        $obtenerIdAlbumes = "select Id from Albumes where IdArtista = $idArt";
        if ($conn->consulta($obtenerIdAlbumes)) {
            $idAlbumes = $conn->restantesRegistros();

            for ($j = 0; $j < count($idAlbumes); $j++) {
                $albActual = $idAlbumes[$j]['Id'];
                $ok = deleteAlbumId($albActual, $conn);
            }   
        }
        $resultado = json_encode("Se ha eliminado correctamente");
    } else {
        $resultado = "Problemas al eliminar";
    }

    $conn->desconectar();
}

function deleteSongId($id, $c) {
    $ret = false;
    $encontrar = "select * from Canciones where Id = $id";
    if ($c->consulta($encontrar)) {
        $temaEncontrado = $c->siguienteRegistro();
        $audioSrc = $temaEncontrado["AudioSource"];
        unlink('/var/www/ObligatorioT2/uploads/' . $audioSrc);
    }
    $borrar = "delete from Canciones where Id = $id";
    if ($c->consulta($borrar)) {
        $eliminarVals = "delete from Valoraciones where CancionId = $id";
        if ($c->consulta($eliminarVals)) {
            $eliminarDePL = "delete from Playlists where IdCancion = $id";
            if ($c->consulta($eliminarDePL)) {
                $ret = true;
            }
        }
        $ret = true;
    }
    return $ret;
}

function deleteAlbumId($id, $c) {
    $ret = false;
    $encontrar = "select * from Albumes where Id = $id";
    if ($c->consulta($encontrar)) {
        $albumEncontrado = $c->siguienteRegistro();
        $imagenSrc = $albumEncontrado['ImagenSource'];
        unlink('/var/www/ObligatorioT2/uploads/' . $imagenSrc);
    }
    $borrar = "delete from Albumes where Id = $id";
    if ($c->consulta($borrar)) {
        $obtenerCanciones = "select Id from Canciones where AlbumId = $id";
        if ($c->consulta($obtenerCanciones)) {
            $result = $c->restantesRegistros();
            for ($i = 0; $i < count($result); $i++) {
                $actual = $result[$i]['Id'];
                $ok = deleteSongId($actual, $c);
            }
        }
        $ret = true;
    } else {
        $ret = false;
    }
    return $ret;
}

echo $resultado;
