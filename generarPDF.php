<?php

require('libs/fpdf.php');
include_once("config_BD.php");

class PDF extends FPDF {

// Tabla simple
    function BasicTable($header, $data) {
        // Cabecera
        foreach ($header as $col)
            $this->Cell(40, 7, $col, 1);
        $this->Ln();
        // Datos
        foreach ($data as $row) {
            foreach ($row as $col)
                $this->Cell(40, 6, utf8_decode($col), 1);
            $this->Ln();
        }
    }

// Una tabla más completa
    function ImprovedTable($header, $data) {
        // Anchuras de las columnas
        $w = array(15, 35, 70, 40);
        // Cabeceras
        for ($i = 0; $i < count($header); $i++)
            $this->Cell($w[$i], 7, $header[$i], 1, 0, 'C');
        $this->Ln();
        // Datos
        foreach ($data as $row) {
            $this->Cell($w[0], 6, number_format($row['id']), 'LR'); //id
            $this->Cell($w[1], 6, $row['fecha'], 'LR');
            $this->Cell($w[2], 6, ($row['titulo']), 'LR', 0, 'R');
            $this->Cell($w[3], 6, utf8_decode($row['categoria']), 'LR', 0, 'R');
            $this->Ln();
        }
        // Línea de cierre
        $this->Cell(array_sum($w), 0, '', 'T');
    }

// Tabla coloreada
    function FancyTable($header, $data, $accion) {
        $this->SetFillColor(255, 0, 0);
        $this->SetTextColor(255);
        $this->SetDrawColor(128, 0, 0);
        $this->SetLineWidth(.3);
        $this->SetFont('', 'B');
        // Cabecera
        if($accion =='usuarios'){
            $w = array(80, 80);
        }else if($accion =='reproducciones'){
            $w = array(60,60,40);
        }
        
        for ($i = 0; $i < count($header); $i++)
            $this->Cell($w[$i], 7, $header[$i], 1, 0, 'C', true);
        $this->Ln();
        // Restauración de colores y fuentes
        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);
        $this->SetFont('');
        // Datos
        $fill = false;
        foreach ($data as $row) {
            //Relleno Tabla
            if ($accion == 'usuarios') {
                $this->Cell($w[0], 6, $row['Nombre'], 'LR', 0, 'L', $fill);
                $this->Cell($w[1], 6, ($row['Mail']), 'LR', 0, 'R', $fill);
                $this->Ln();
                $fill = !$fill;
            } else if ($accion == 'reproducciones') {
                $this->Cell($w[0], 6, $row['Nombre'],'LR',0,'L',$fill);
                $this->Cell($w[1], 6, $row['Pais'], 'LR', 0, 'L', $fill);
                $this->Cell($w[2], 6, ($row['Reproducciones']), 'LR', 0, 'R', $fill);
                $this->Ln();
                $fill = !$fill;
            }
        }
        $this->Cell(array_sum($w), 0, '', 'T');
        // Línea de cierre
    }

}//FIN CLASE PFD


$pdf = new PDF();

$accion = (strlen(filter_input(INPUT_GET, 'accion'))) ? filter_input(INPUT_GET, 'accion') : '';
$result = array();
$header = array();
$sql = "";

if ($accion == 'usuarios') {
    $header = array('Nombre', 'Mail');
    $sql = "select Nombre,Mail from Usuarios order by Nombre asc";
} else if ($accion == 'reproducciones') {
    $header = array('Nombre', 'Pais', 'Reproducciones');
    $sql = "select a.Nombre, a.Pais , COALESCE(sum(c.Reproducciones),0) as Reproducciones
            from Artistas a
            left join Albumes b on b.IdArtista = a.Id
            left join Canciones c on c.AlbumId = b.Id
            group by a.Id,a.Nombre order by Reproducciones desc";
}

$conn->conectar();

if ($conn->consulta($sql)) {
    $result = $conn->restantesRegistros();
}
$conn->desconectar();

$pdf->SetFont('Arial', '', 14);
$pdf->AddPage();
$pdf->FancyTable($header, $result, $accion);

if($accion=='reproducciones'){
    $pdf->Output('reproducciones.pdf','D');
}else{
    $pdf->Output('usuarios.pdf','D');
}

?>