{* Smarty *}
<!DOCTYPE html>
<html>
    <title>Registro</title>
    <meta charset="utf-8"/> 
    <head>
        <link rel="stylesheet" href="css/estilos.css" type="text/css" />
        <script type="text/javascript" src="js/jquery-2.2.0.js"></script>
        <!-- <script type="text/javascript" src="js/funciones.js"></script> -->
    </head>
    <body>  
        {if $error}
            <div class="error">
                {$error}
            </div>
        {/if}
        <form action="?" method="post">
            <h2>Registro</h2>
            <div>
                <label>Nombre</label> <input name="nombre" />
            </div>
            <div>
                <label>Apellido</label> <input name="apellido" />
            </div>
            <div>
                <label>Mail</label> <input name = "mail" />
            </div>
            <div>
                <label>Password</label> <input type="password" name="pass" />
            </div>
            <input type="hidden" name="accion" value="register" />
            <button type="submit">Registrar</button>
        </form>
    </body>
</html>
