{* Smarty *}
<!DOCTYPE html>
<html>
    <title>Login</title>
    <meta charset="utf-8"/> 
    <head>
        <link rel="stylesheet" href="css/estilos.css" type="text/css" />
        <script type="text/javascript" src="js/jquery-2.2.0.js"></script>
       <!-- <script type="text/javascript" src="js/funciones.js"></script> -->
    </head>
    <body> 
        {if $error}
            <div class="error">
                {$error}
            </div>
        {/if}
        {if $usuario.nombre}
        
           <div>
                Bienvenido {$usuario.nombre}
                <p>
                    <a href="?accion=logout">Salir</a>
                </p>
           </div>
        {else}
            <form action="?" method="post">
                <h2>Login</h2>
                <div>
                    <input name="user" placeholder="Usuario"/>
                </div>
                <div>
                    <input type="password" name="pass" placeholder="Password"/>
                </div>
                <input type="hidden" name="accion" value="login" />
                <button type="submit">Login</button>
            </form>
            <div>
                <span>No esta registrado?</span> <a href = "registro.php">Registrarse</a> 
            </div>
        {/if}
    </body>
</html>
