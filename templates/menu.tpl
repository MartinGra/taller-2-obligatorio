{* Smarty *}
<!DOCTYPE html>
<html>
    <title>Menu</title>
    <meta charset="utf-8"/> 
    <head>
        <link rel="stylesheet" href="css/estilos.css" type="text/css"/>
        <script type="text/javascript" src="js/jquery-2.2.0.js"></script>
        <script type="text/javascript" src="js/funciones.js"></script>

        <!-- Libs de Fonts y acciones de rating estrellas -->
        <script src="js/prefixfree.min.js"></script>
        <script src="js/modernizr.js" type="text/javascript"></script>
        <link href="css/normalize.css" rel="stylesheet"> 
        <link href='css/font-awesome.min' rel='stylesheet prefetch'>
        <!--FIN Libs externas-->

        <!--Alertify-->
        <link rel="stylesheet" href="css/alertify.core.css" />
        <link rel="stylesheet" href="css/alertify.default.css"/>
        <script src="js/alertify.min.js"></script>
        <!--Fin Alertify-->
    </head>
    <body> 
        <br>

        {if $usuario.mail == 'administrador'}
            {if $error}
                <div class="error">
                    {$error}
                </div>
            {/if} 
            <div class="loggout"> 
                <p>
                    <a href="?accion=logout">Logout</a>
                </p>
            </div>
            <div>
                <img src="imgs/cuenta.png" height="30" width="30"> {$usuario.nombre}
            </div>
            <div> <!--OPCION 1 MENU -->
                <h4><a href="#" id="c_AB_artistas"> 1) Mantenimiento de Artistas </a></h4>               
                <div id="AB_artistas">             
                    <form action='?' method='post'> <!--ALTA de artista-->
                        <div><strong>Ingrese datos del artista/grupo:</strong></div>
                        <div><label>Nombre</label> <input type='text' name='artist'/></div>
                        <div><label>Pais de origen</label> <input type='text' name="country"/></div>
                        <input type="hidden" name="accion" value="registrarArtista"/>
                        <button type="submit">Agregar</button>
                    </form>
                    <!--BAJA artista-->
                    <br>
                    <div>
                        <div><strong>Baja Artista:</strong></div>
                        <div>
                            <input type='text' id='busqueda_art' placeholder='A borrar'> <button id="buscar_art_b">Buscar</button>                           
                            <ul id="artistas_a_borrar">
                            </ul>
                            <ul id="c_paginador_art">
                                <li id="paginador_art"></li>
                                <button id="limpiar_artistas">Limpiar Busqueda</button>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div><!--OPCION 2 MENU -->
                <h4><a href="#" id="c_AB_albumes"> 2) Mantenimiento de Albumes</a></h4>
                <div id="AB_albumes">
                    <form action='?' method='post' enctype="multipart/form-data"><!--ALTA de albumes-->
                        <div><strong>Ingrese datos del album:</strong></div>
                        <div><label>Nombre</label><input type='text' name='album'/></div>
                        <div><label>Codigo artista</label><input type='text' name='artistId'/></div>
                        <div>
                            <label>Imagen album </label> 
                            <input type="file" name = "imagen" accept='image/*'/><br/>
                        </div>
                        <div><label>Año publicación</label><input type='text' name='year'/></div>
                        <input type="hidden" name="accion" value="registrarAlbum"/>
                        <button type="submit">Agregar</button>
                    </form>
                    <!-- Baja albumes-->
                    <br>
                    <div>
                        <div><strong>Baja Albumes:</strong></div>
                        <div>
                            <input type='text' id='busqueda_alb' placeholder='A borrar'> <button id="buscar_alb_b">Buscar</button>                           
                            <ul id="albumes_a_borrar">
                            </ul>
                            <ul id="c_paginador_alb">
                                <li id="paginador_alb"></li>
                                <button id="limpiar_albumes">Limpiar Busqueda</button>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div><!--OPCION 3 MENU -->
                <h4><a href="#" id="c_AB_temas">3) Mantenimiento de Temas</a></h4>
                <div id="AB_temas">
                    <!-- Alta temas -->
                    <form action='?' method='post' enctype='multipart/form-data'>
                        
                        <div><strong>Ingrese datos de la cancion:</strong></div>
                        <div><label>Nombre</label><input type='text' name='song'/></div>                        
                        <div><label>Codigo album</label><input type='text' name='albumId'/></div>
                        <div>
                            <label>Duracion</label>
                            <select name="minutos"></select>:<select name='segundos'></select>
                        </div>
                        <div>
                            <label>Cancion MP3 </label>
                            <input type="file" name="cancion" accept='audio/*'/><br/>
                        </div>
                        <input type="hidden" name="accion" value="registrarTema"/>
                        <button type="submit">Agregar</button>
                    </form>
                    <!-- Baja temas -->
                    <br>
                    <div>
                        <div><strong>Baja Tema:</strong></div>
                        <div>
                            <input type='text' id='a_buscar' placeholder='A borrar'> <button id="buscar_tema_b">Buscar</button>                           
                            <ul id="temas_a_borrar">
                            </ul>
                            <ul id="c_paginador_t">
                                <li id="paginador_t"></li>
                                <button id="limpiar_temas">Limpiar Busqueda</button>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
            <div><!--OPCION 4 MENU -->
                <h4><a href='#' id="c_reportes">4) Reportes</a></h4>
                <div id="reportes">

                    <!-- Reporte usuarios -->
                    <div>Mostrar usuarios<a href = "#" id="mostrar_rep_usuarios"><img src="imgs/usuarios" height="30" width="30"></a></div>
                    <div id="contenido_reporte_usuarios" class="contenido_reporte_us">   
                        <table id="rep_usuarios" width="40%" height="50px" border="1"></table>
                        <table id="c_paginador_us" width="40%" height="50px">
                            <tr> 
                                <td id="paginador_us"></td>
                                <td style="float:right"><button id="limpiar_usuarios">Cerrar reporte</button></td> <!--Se tiene que ir el style-->
                            </tr>
                            <tr>
                                <td><td><a style="float:right" href="generarPDF.php?accion=usuarios">Descargar</a><td/>
                            </tr>
                        </table>    
                    </div>
                    <br>
                    <!-- Reporte reproducciones-->
                    <div>Mostrar reproducciones<a href="#" id="mostrar_rep_reproducciones"><img src="imgs/reproducciones" height="30" width="30"></a></div>
                    <div id="contenido_reporte_reproducciones" class="contenido_reporte_reps">
                        <table id="rep_reproducciones" width="40%" height="50px" border="1"></table>
                        <table id="c_paginador_rep" width="40%" height="50px">
                            <tr> 
                                <td id="paginador_rep"></td>
                                <td style="float:right"><button id="limpiar_reproducciones">Cerrar reporte</button></td> <!--Se tiene que ir el style-->
                            </tr>
                            <tr>
                                <td><td><a style="float:right" href="generarPDF.php?accion=reproducciones">Descargar</a><td/>
                            </tr>
                        </table>        
                    </div>
                </div>
            </div>
            
        {else}
            <div class="usuarioNombre">
                <img src="imgs/cuenta.png" height="30" width="30"> {$usuario.nombre}
            </div>
            <div class="contenedorBusqueda">
                <input type='text' id='aBuscar' placeholder='Buscar'>
                <button id="buscador"><img src="imgs/buscador.png" width="15" height="15"></button>
            </div>
            <div class="loggout"> 
                <p>
                    <a href="?accion=logout">Logout</a>
                </p>
            </div>
            <br>
            <div class="contenedorPlaylists">
                <lablel>Playlists</lablel>
                <ul id="playlists_usuario">
                </ul>
            </div>
            <div class="contenedorResultados">
                <table id="resultados_busqueda" height="50px" border="1"></table>
                <table id="c_paginador_res" height="50px">
                    <tr>
                        <td id ="paginador_res"></td>
                    </tr>
                </table>
            </div>
            <div class="contenedorResultadosPlaylist">
                <table id="resultados_playlist" height="50px" border="1">
                </table>
                <table id="c_paginador_pl" height="50px">
                    <tr>
                        <td id ="paginador_pl"></td>
                    </tr>
                </table>
            </div>
        {/if}
    </body>
</html>