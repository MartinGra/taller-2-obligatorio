<?php
session_start();

require 'smarty.php';
require 'config_BD.php';

if(!is_array($_SESSION['user'])){
    $_SESSION['user'] = array(
        "id" => -1,
        "nombre" => "",
        "mail" => ""
    );
}

$accion = strlen(filter_input(INPUT_POST, 'accion')) ? filter_input(INPUT_POST, 'accion') : filter_input(INPUT_GET, 'accion');
$usuario = strlen(filter_input(INPUT_POST, 'user')) ? filter_input(INPUT_POST, 'user') : filter_input(INPUT_GET, 'user');
$password = strlen(filter_input(INPUT_POST, 'pass')) ? filter_input(INPUT_POST, 'pass') : filter_input(INPUT_GET, 'pass');

if($accion == "login"){
    $sql = "select * from Usuarios where Mail = :mail and Password = :pass";
    
    $parametros = array();
    $parametros[] = array("mail", $usuario, "string");
    $parametros[] = array("pass", $password, "string");
    
    $conn->conectar();
    if($conn->consulta($sql, $parametros)){
      $user = $conn->siguienteRegistro();

        if(is_array($user)){
            $_SESSION['user']['id'] = $user['Id'];
            $_SESSION['user']['nombre'] = $user['Nombre'];
            $_SESSION['user']['mail'] = $user['Mail'];
            
            var_dump($_SESSION);
            
            header("Location: menu.php");
            die();
        } else {
            $error = "Usuario inválido, reintente.";
        }
    } else {
        $error = "Error general, reintente.";
    }
    
    $conn->desconectar();
    
} else if($accion == "logout") {
    unset($_SESSION['user']);
    session_destroy();
    header("Location: " . filter_input(INPUT_SERVER, 'PHP_SELF', FILTER_SANITIZE_STRING));//$_SERVER["PHP_SELF"]);
    die();
}

if($_SESSION['user']["id"] > -1){
    $smarty->assign("usuario", $_SESSION['user']); 
}
if(strlen($error)){
    $smarty->assign("error", $error);
}

$smarty->display("index.tpl");
