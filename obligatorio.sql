-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 16, 2016 at 06:44 PM
-- Server version: 5.5.43
-- PHP Version: 5.4.4-14+deb7u5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `obligatorio`
--

-- --------------------------------------------------------

--
-- Table structure for table `Albumes`
--

CREATE TABLE IF NOT EXISTS `Albumes` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdArtista` int(11) NOT NULL,
  `Nombre` varchar(255) NOT NULL,
  `ImagenSource` varchar(255) NOT NULL,
  `Año` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `Albumes`
--

INSERT INTO `Albumes` (`Id`, `IdArtista`, `Nombre`, `ImagenSource`, `Año`) VALUES
(1, 1, 'Nimrod', '20160306195043_Green_Day_-_Nimrod_cover.jpg', 1997),
(5, 5, 'For Lack of a Better Name', '20160313123838_For_Lack_of_a_Better_Name_Bonus_Track_Version_floabn.png', 2009),
(6, 5, 'Random Album Title', '20160313124300_RandomAlbumTitle.png', 2008),
(7, 2, 'Nightmare', '20160313125450_Avenged_sevenfol_529e455d44346.jpg', 2010),
(8, 2, 'Hail to the King', '20160313125523_Hail_to_the_King_Album_Art.jpg', 2013),
(9, 4, 'Eco', '20160313131020_R-3487682-1332343967.jpeg.jpg', 2004),
(10, 16, 'The Illusion of Progress', '20160313141355_51TonJshq1L.jpg', 2008),
(11, 16, '14 Shades of Grey', '20160313141431_Staind_14_Shades_of_Grey.jpg', 2003),
(12, 16, 'Break the Cycle', '20160313141511_41N62S4GKRL.jpg', 2001),
(13, 6, 'The Black Album', '20160313143024_Metallica (Black Album).jpg', 1991),
(14, 7, 'Hot', '20160313143805_51avvNZz4uL._SY300_.jpg', 2009),
(15, 7, 'I Am the Club Rocker', '20160313143838_inna_club_rocker_cover.jpg', 2011),
(16, 8, 'V', '20160313155527_Maroon5VAlternateCover.jpg', 2014),
(17, 9, 'Heathen Chemistry', '20160313160556_51wQVbbSkpL.jpg', 2002),
(19, 10, 'Appetite for Destruction', '20160313162510_71dkjEG4IwL._SL1200_.jpg', 1987),
(29, 18, 'Vol. 3: (The Subliminal Verses)', '20160314141835_Slipknot_-_Vol._3-_(The_Subliminal_Verses).jpg', 2004),
(30, 18, 'Slipknot', '20160314142015_SlipknotOriginal2.jpg', 1999),
(31, 11, 'Era Tranquila', '20160315124446_Marama-Era-Tranquila-Diciembre-2015.jpg', 2014),
(32, 12, 'De Fiesta', '20160315175333_nHtlnoXt.jpg', 2014),
(33, 9, '(What''s the Story) Morning Glory?', '20160315175424_Oasis_-_(Whats_The_Story)_Morning_Glory_album_cover.jpg', 1995);

-- --------------------------------------------------------

--
-- Table structure for table `Artistas`
--

CREATE TABLE IF NOT EXISTS `Artistas` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(255) NOT NULL,
  `Pais` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `Artistas`
--

INSERT INTO `Artistas` (`Id`, `Nombre`, `Pais`) VALUES
(1, 'Green Day', 'Estados Unidos'),
(2, 'Avenged Sevenfold', 'Estados Unidos'),
(3, 'Attaque 77', 'Argentina'),
(4, 'Jorge Drexler', 'Uruguay'),
(5, 'Deadmau5', 'Canada'),
(6, 'Metallica', 'Estados Unidos'),
(7, 'Inna', 'Rumania'),
(8, 'Maroon 5', 'Estados Unidos'),
(9, 'Oasis', 'Inglaterra'),
(10, 'Guns N'' Roses', 'Estados Unidos'),
(11, 'Marama', 'Uruguay'),
(12, 'Rombai', 'Uruguay'),
(13, 'Taylor Swift', 'Estados Unidos'),
(14, 'Coldplay', 'Inglaterra'),
(15, 'Link Park', 'Estados Unidos'),
(16, 'Staind', 'Estados Unidos'),
(18, 'Slipknot', 'Estados Unidos');

-- --------------------------------------------------------

--
-- Table structure for table `Canciones`
--

CREATE TABLE IF NOT EXISTS `Canciones` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(255) NOT NULL,
  `AlbumId` int(11) NOT NULL,
  `Duration` varchar(10) NOT NULL,
  `Reproducciones` int(11) NOT NULL,
  `AudioSource` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=74 ;

--
-- Dumping data for table `Canciones`
--

INSERT INTO `Canciones` (`Id`, `Nombre`, `AlbumId`, `Duration`, `Reproducciones`, `AudioSource`) VALUES
(1, 'Good Riddance (Time Of Your Life)', 1, '02:31', 11, '20160306215504_Good Riddance (Time Of Your Life).mp3'),
(6, 'Nice Guys Finish Last', 1, '04:08', 7, '20160309002603_Nice Guys Finish Last - Green Day.mp3'),
(7, 'Redundant', 1, '03:22', 7, '20160309003123_Green Day - Redundant.mp3'),
(8, 'Ghosts n Stuff', 5, '06:02', 5, '20160313124057_Deadmau5 - Ghosts n Stuff.mp3'),
(9, 'Some Chords', 5, '04:26', 5, '20160313124146_Deadmau5 - Some Chords (HD).mp3'),
(10, 'Strobe', 5, '05:05', 4, '20160313124222_deadmau5 - Strobe (Live Version) (Cover Art).mp3'),
(11, 'I Remember', 6, '04:43', 2, '20160313124356_Deadmau5 FT. Kaskade - I Remember.mp3'),
(14, 'Deseo', 9, '03:45', 5, '20160313131251_Jorge Drexler - Deseo.mp3'),
(15, 'Eco', 9, '03:23', 5, '20160313131325_Jorge Drexler - Eco.mp3'),
(16, 'Todo se transforma', 9, '03:42', 3, '20160313131403_Jorge Drexler - Todo se transforma (video clip).mp3'),
(17, 'Buried Alive', 8, '06:46', 11, '20160313131509_Avenged Sevenfold - Buried Alive [Lyric Video].mp3'),
(18, 'Hail To The King', 8, '05:12', 7, '20160313131613_Avenged Sevenfold - Hail To The King [Official Music Video].mp3'),
(19, 'Nightmare', 8, '06:15', 3, '20160313131658_Avenged Sevenfold - Nightmare [Official Music Video].mp3'),
(20, 'Shepherd Of Fire ', 8, '05:24', 3, '20160313131813_Avenged Sevenfold - Shepherd Of Fire [Official Music Video].mp3'),
(21, 'So Far Away', 8, '05:29', 2, '20160313131909_Avenged Sevenfold - So Far Away [HD].mp3'),
(22, 'So Far Away', 11, '03:58', 2, '20160313141618_Staind- So far away lyrics.mp3'),
(23, 'The Way I Am', 10, '04:18', 3, '20160313142054_Staind- The way I am.mp3'),
(24, 'It''s Been Awhile', 12, '04:24', 2, '20160313142203_Staind-Its Been Awhile.mp3'),
(25, 'Enter Sandman', 13, '05:30', 10, '20160313143112_metallica - enter sandman.mp3'),
(26, 'Nothing Else Matters', 13, '06:25', 5, '20160313143146_Metallica - Nothing Else Matters [Official Music Video].mp3'),
(27, 'Amazing', 14, '03:31', 10, '20160313145124_Inna - Amazing.mp3'),
(28, 'Hot', 14, '03:41', 3, '20160313145156_Inna - Hot (Official Video HD).mp3'),
(29, 'Sun Is Up', 15, '03:28', 3, '20160313145236_Inna - Sun Is Up Lyrics.mp3'),
(30, 'It Was Always You', 16, '03:56', 2, '20160313155640_Maroon 5 - It Was Always You (Audio).mp3'),
(31, 'Sugar', 16, '03:52', 7, '20160313155724_Maroon 5 - Sugar (lyrics).mp3'),
(36, 'Little By Little', 17, '03:59', 3, '20160313161739_Oasis - Little By Little.mp3'),
(37, 'Stop Crying Your Heart Out', 17, '04:54', 2, '20160313161822_Oasis - Stop Crying Your Heart Out.mp3'),
(61, 'Duality', 29, '03:34', 4, '20160314142537_Slipknot - Duality (Official Music Video).mp3'),
(62, 'Todo Comenzo Bailando', 31, '02:27', 3, '20160315124542_MARAMA -  TODO COMENZO BAILANDO.mp3'),
(63, 'Nena', 31, '02:18', 2, '20160315124614_Marama - Nena (vídeo oficial).mp3'),
(64, 'Sweet Child O Mine', 19, '06:48', 1, '20160315175729_Guns N Roses - Sweet Child O Mine.mp3'),
(65, 'Welcome To The Jungle', 19, '04:59', 0, '20160315175816_Guns N Roses - Welcome To The Jungle.mp3'),
(66, 'Paradise City', 19, '06:48', 1, '20160315175844_Guns N Roses-Paradise City (Audio Only).mp3'),
(67, 'Curiosidad', 32, '02:48', 3, '20160315175948_Rombai - Curiosidad.mp3'),
(68, 'Yo Te Propongo', 32, '02:46', 0, '20160315180019_Rombai - Yo te propongo.mp3'),
(69, 'Noche Loca', 32, '03:31', 1, '20160315180052_Rombai Ft. Marama - Noche Loca (Video Oficial).mp3'),
(70, 'Acquiesce', 33, '04:37', 9, '20160315180126_Oasis - Acquiesce.mp3'),
(71, 'Champagne Supernova', 33, '07:28', 1, '20160315180152_Oasis - Champagne Supernova.mp3'),
(72, 'Wonderwall', 33, '04:37', 1, '20160315180306_Oasis - Wonderwall.mp3'),
(73, 'This Summer', 16, '03:34', 2, '20160315180623_Maroon 5 - This Summers Gonna Hurt Like A Motherf****r.mp3');

-- --------------------------------------------------------

--
-- Table structure for table `Playlists`
--

CREATE TABLE IF NOT EXISTS `Playlists` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `MailUsuario` varchar(255) NOT NULL,
  `Nombre` varchar(255) NOT NULL,
  `IdCancion` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `Playlists`
--

INSERT INTO `Playlists` (`Id`, `MailUsuario`, `Nombre`, `IdCancion`) VALUES
(1, 'marting77@outlook.com', 'Rock', 1),
(2, 'jul@yahoo.com', 'Rock', 1),
(3, 'marting77@outlook.com', 'Rock', 6),
(4, 'marting77@outlook.com', 'Cumbia', 63);

-- --------------------------------------------------------

--
-- Table structure for table `Usuarios`
--

CREATE TABLE IF NOT EXISTS `Usuarios` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(255) NOT NULL,
  `Mail` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `Usuarios`
--

INSERT INTO `Usuarios` (`Id`, `Nombre`, `Mail`, `Password`) VALUES
(1, 'Martin Gra', 'marting77@gmail.com', 'marTIN123,./'),
(2, 'Julio Amorin', 'jul@yahoo.com', 'aA123kl.,'),
(3, 'Martin Graña', 'marting77@outlook.com', 'marTIN12,.'),
(4, 'Camila Varela', 'cam_var@hotmail.com', 'camVAR1.'),
(5, 'James Bond', 'jbond@gmail.com', 'jB1,'),
(6, 'Paula Oliver', 'pau_oli@hotmail.com', 'pauO21.'),
(7, 'Lautaro Josema', 'laut@outlook.com', 'lA1,'),
(8, 'Marta Lara', 'marta@gmail.com', 'marT12.'),
(9, 'Luis Suarez', 'lucho@fifa.com', 'mSn9/'),
(15, 'jorge ser', 'jor@gmail.com', 'jOR21.,'),
(16, 'Is Abel', 'isa@gmail.com', 'ISab.,21'),
(17, 'Batman Robin', 'bat@gmail.com', 'bAT123,.'),
(20, 'Josema Listorti', 'esmagico@gmail.com', 'oOH12,.a'),
(21, 'Moby Dick', 'mob@yahoo.com', 'kAM,21.,a'),
(22, 'Neymar Jr', 'hey@mail.br', 'barCe12,.'),
(26, 'Im Done', 'Jesus@mail.com', 'Christ12,.a'),
(27, 'Alex Mo', 'alx@gmail.com', 'aLeax,1.21sx'),
(28, 'Mabel Martinez', 'mab40@gmail.com', 'mA1,.ab'),
(29, 'Admin', 'administrador', 'Admin1234!');

-- --------------------------------------------------------

--
-- Table structure for table `Valoraciones`
--

CREATE TABLE IF NOT EXISTS `Valoraciones` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `CancionId` int(11) NOT NULL,
  `MailUsuario` varchar(255) NOT NULL,
  `Rating` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=45 ;

--
-- Dumping data for table `Valoraciones`
--

INSERT INTO `Valoraciones` (`Id`, `CancionId`, `MailUsuario`, `Rating`) VALUES
(3, 1, 'marting77@outlook.com', 5),
(4, 1, 'jul@yahoo.com', 3),
(5, 70, 'jul@yahoo.com', 4),
(6, 17, 'marting77@outlook.com', 4),
(7, 67, 'marting77@outlook.com', 2),
(8, 15, 'marting77@outlook.com', 2),
(9, 61, 'marting77@outlook.com', 1),
(10, 8, 'marting77@outlook.com', 4),
(11, 25, 'marting77@outlook.com', 5),
(12, 27, 'marting77@outlook.com', 1),
(13, 71, 'marting77@outlook.com', 4),
(14, 14, 'marting77@outlook.com', 3),
(15, 6, 'marting77@outlook.com', 3),
(16, 7, 'marting77@outlook.com', 5),
(17, 18, 'marting77@outlook.com', 3),
(18, 28, 'marting77@outlook.com', 2),
(19, 11, 'marting77@outlook.com', 3),
(20, 30, 'marting77@outlook.com', 5),
(21, 24, 'marting77@outlook.com', 3),
(22, 36, 'marting77@outlook.com', 4),
(23, 63, 'marting77@outlook.com', 2),
(24, 19, 'marting77@outlook.com', 4),
(25, 69, 'marting77@outlook.com', 2),
(26, 26, 'marting77@outlook.com', 4),
(27, 66, 'marting77@outlook.com', 3),
(28, 20, 'marting77@outlook.com', 4),
(29, 21, 'marting77@outlook.com', 3),
(30, 22, 'marting77@outlook.com', 2),
(31, 9, 'marting77@outlook.com', 4),
(32, 37, 'marting77@outlook.com', 2),
(33, 10, 'marting77@outlook.com', 4),
(34, 31, 'marting77@outlook.com', 3),
(35, 29, 'marting77@outlook.com', 1),
(36, 64, 'marting77@outlook.com', 4),
(37, 23, 'marting77@outlook.com', 3),
(38, 73, 'marting77@outlook.com', 5),
(39, 62, 'marting77@outlook.com', 3),
(40, 16, 'marting77@outlook.com', 4),
(41, 65, 'marting77@outlook.com', 3),
(42, 72, 'marting77@outlook.com', 2),
(43, 68, 'marting77@outlook.com', 1),
(44, 70, 'marting77@outlook.com', 4);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
