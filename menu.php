<?php

require 'smarty.php';
require 'config_BD.php';

session_start();

$accion = strlen(filter_input(INPUT_POST, 'accion')) ? filter_input(INPUT_POST, 'accion') : filter_input(INPUT_GET, 'accion');

if ($accion == "logout") {
    unset($_SESSION['user']);
    session_destroy();
    header("Location: index.php");
    die();
} else if ($accion == "registrarArtista") {

    $nombre_artista = strlen(filter_input(INPUT_POST, 'artist')) ? filter_input(INPUT_POST, 'artist') : '';
    $pais_origen = strlen(filter_input(INPUT_POST, 'country')) ? filter_input(INPUT_POST, 'country') : '';

    if (!textoVacio($nombre_artista) && !textoVacio($pais_origen)) {
        $unico = "select * from Artistas where Nombre = :nom and Pais= :pai";
        $params = array();
        $params[] = array('nom', $nombre_artista, "string");
        $params[] = array('pai', $pais_origen, "string");

        $conn->conectar();
        if ($conn->consulta($unico, $params) && !is_array($conn->siguienteRegistro())) { //Es unico
            $insertar = "insert into Artistas (Nombre,Pais) values (:nom,:pai)";
            if ($conn->consulta($insertar, $params)) {
                $error= $nombre_artista . " agregado correctamente";
            } else {
                $error= $conn->ultimoError();
            }
        } else {
            $error= "Artista ya ingresado.";
        }
        $conn->desconectar();
    } else {
        $error= "PARAMETRO/S Vacios";
    }
} else if ($accion == "registrarAlbum") {

    $nombre_album = strlen(filter_input(INPUT_POST, 'album')) ? filter_input(INPUT_POST, 'album') : '';
    $id_artista = strlen(filter_input(INPUT_POST, 'artistId')) ? filter_input(INPUT_POST, 'artistId') : '';
    $year = strlen(filter_input(INPUT_POST, 'year')) ? filter_input(INPUT_POST, 'year') : '';
    //imagen
    $nombre_archivo = date("YmdHis") . "_" . $_FILES['imagen']['name'];
    $tamanio_archivo = $_FILES['imagen']['size'];
    $nombre_temporal = $_FILES['imagen']['tmp_name'];

    $conn->conectar();

    //validacion de campos
    if (campoNumerico($year) && !textoVacio($nombre_album) && campoNumerico($id_artista) && !textoVacio($_FILES['imagen']['name']) && existeIdArtista($id_artista, $conn)) {
        $unico = "select * from Albumes where IdArtista = :idA and Nombre = :nom and ImagenSource = :img and Año = :yea";

        $params = array();
        $params[] = array('idA', $id_artista, "int");
        $params[] = array('nom', $nombre_album, "string");
        $params[] = array('img', $nombre_archivo, "string");
        $params[] = array('yea', $year, "int");

        if ($conn->consulta($unico, $params) && !is_array($conn->siguienteRegistro())) {//Album unico
            if (is_uploaded_file($nombre_temporal)) {
                if ($tamanio_archivo > 1000000) {
                    $error= "Tamaño incorrecto, 1mb max";
                } else {
                    if (move_uploaded_file($nombre_temporal, "uploads/" . $nombre_archivo)) {
                        $insertar = "insert into Albumes (IdArtista,Nombre,ImagenSource,Año) values(:idA,:nom,:img,:yea)";
                        if ($conn->consulta($insertar, $params)) {
                            $error= $nombre_album . " agregado correctamente";
                        } else {
                            $error= $conn->ultimoError();
                        }
                    } else {
                        $error= "error en el fichero";
                    }
                }
            }
        } else {
            $error= "album no unico";
        }
        $conn->desconectar();
    } else {
        $error= "Ningun campo puede ser vacio y el codigo del artista debe ser valido";
    }
} else if ($accion == "registrarTema") {
    $nombre_cancion = strlen(filter_input(INPUT_POST, 'song')) ? filter_input(INPUT_POST, 'song') : '';
    $codigo_album = strlen(filter_input(INPUT_POST, 'albumId')) ? filter_input(INPUT_POST, 'albumId') : '';
    $minutos = strlen(filter_input(INPUT_POST, 'minutos')) ? filter_input(INPUT_POST, 'minutos') : '';
    $segundos = strlen(filter_input(INPUT_POST, 'segundos')) ? filter_input(INPUT_POST, 'segundos') : '';
    $reproducciones = 0;
    //audio
    $nombre_archivo = date("YmdHis") . "_" . $_FILES['cancion']['name'];
    $tamanio_archivo = $_FILES['cancion']['size'];
    $nombre_temporal = $_FILES['cancion']['tmp_name'];

    $conn->conectar();

    //validacion de campos
    if (!textoVacio($nombre_cancion) && campoNumerico($codigo_album) && $minutos != "mm" && $segundos != "ss" && !textoVacio($_FILES['cancion']['name']) && existeIdAlbum($codigo_album, $conn)) {
        $unica = "select * from Canciones where Nombre= :nom and AlbumId = :idA"
                . " and Duration = :dur and Reproducciones = :rep and AudioSource= :aud";

        $params = array();
        $params[] = array('nom', $nombre_cancion, "string");
        $params[] = array('idA', $codigo_album, "int");
        $params[] = array('dur', $minutos . ":" . $segundos, "string");
        $params[] = array('rep', $reproducciones, "int");
        $params[] = array('aud', $nombre_archivo, "string");

        //Validacion cancion unica
        if ($conn->consulta($unica, $params) && !is_array($conn->siguienteRegistro())) {
            if (is_uploaded_file($nombre_temporal)) {
                if ($tamanio_archivo > 10000000) {
                    $error= "Tamaño incorrecto, 10mb max";
                } else {
                    if (move_uploaded_file($nombre_temporal, "uploads/" . $nombre_archivo)) {
                        $insertar = "insert into Canciones (Nombre,AlbumId,Duration,Reproducciones,AudioSource) values(:nom,:idA,:dur,:rep,:aud)";
                        if ($conn->consulta($insertar, $params)) {
                            $error= $nombre_cancion . " agregada correctamente";
                        } else {
                            $error= $conn->ultimoError();
                        }
                    } else {
                        $error="error en el fichero";
                    }
                }
            }
        } else {
            $error= "Error: Ya fue ingresada";
        }
        $conn->desconectar();
    } else {
        $error= "Campos no pueden ser vacios, y codigo del album debe ser valido";
    }
}

function textoVacio($field) {
    return $field == '';
}

function campoNumerico($field) {
    return ctype_digit($field);
}

function existeIdArtista($idA, $c) {
    $sql = "select count(*) as Cantidad from Artistas where Id = $idA";
    if ($c->consulta($sql)) {
        return ($c->siguienteRegistro()['Cantidad'] > 0) ? true : false;
    } else {
        return false;
    }
}

function existeIdAlbum($idA, $c) {
    $sql = "select count(*) as Cantidad from Albumes where Id = $idA";
    if ($c->consulta($sql)) {
        return ($c->siguienteRegistro()['Cantidad'] > 0) ? true : false;
    } else {
        return false;
    }
}

if(strlen($error)){
    $smarty->assign("error", $error);
}

$smarty->assign("usuario", $_SESSION['user']);
$smarty->display("menu.tpl");
