//Variables
var cantidadDePaginasCan = 0;
var paginaCan = 0;
var cantidadDePaginasAlb = 0;
var paginaAlb = 0;
var cantidadDePaginasArt = 0;
var paginaArt = 0;
var cantidadDePaginasUs = 0;
var paginaUs = 0;
var cantidadDePaginasRep = 0;
var paginaRep = 0;
var cantidadDePaginasBus = 0;
var paginaBus = 0;
var cantidadDePaginasPL = 0;
var paginaPL = 0;

//Me va a permitir saber cuando le doy play a una cancion si cambie de tema actual para actualizar reproducciones
var temaActual = 0;
//Asi al actualizar las reproducciones o valoracion de un tema. Cargo las canciones de la ultima busqueda
var ultimaBusqueda = "";
var mailActual = "";
var playlistActual = "";

$(document).ready(function () {
    //ocultar menues por defecto
    $('#AB_artistas').hide();
    $('#AB_albumes').hide();
    $('#AB_temas').hide();
    $('#reportes').hide();
    $('#resultados_busqueda').hide();
    
    //hide a selector de playlists
    ocultarBajaTemas();
    ocultarBajaAlbumes();
    ocultarBajaArtistas();
    

    //Reporte usuarios
    $('#mostrar_rep_usuarios').click(function () {
        paginaUs = 0;
        cargarReporteUsuarios(paginaUs);
        $('#contenido_reporte_usuarios').show();
    });
    //Reporte reproducciones
    $('#mostrar_rep_reproducciones').click(function () {
        paginaRep = 0;
        cargarReporteReproducciones(paginaRep);
        $('#contenido_reporte_reproducciones').show();
    });

    //rellenar selects
    cargarMinutosYSegundos();

    //toggle display
    $('#c_AB_artistas').click(function () {
        $('#AB_artistas').toggle('fast');
    });
    $('#c_AB_albumes').click(function () {
        $('#AB_albumes').toggle('fast');
    });
    $('#c_AB_temas').click(function () {
        $('#AB_temas').toggle('fast');
    });
    $('#c_reportes').click(function () {
        $('#reportes').toggle('fast');
        $('#contenido_reporte_usuarios').hide();
        $('#contenido_reporte_reproducciones').hide();
    });

    //Botones buscadores de elementos
    $("#buscador").click(function (e) {
        e.preventDefault();
        paginaBus = 0;
        busquedaPrincipal(paginaBus, "");
    });
    $("#buscar_art_b").click(function (e) {
        e.preventDefault();
        paginaArt = 0;
        buscarArtistaABorrar(paginaArt);
    });
    $("#buscar_alb_b").click(function (e) {
        e.preventDefault();
        paginaAlb = 0;
        buscarAlbumABorrar(paginaAlb);
    });
    $("#buscar_tema_b").click(function (e) {
        e.preventDefault();
        paginaCan = 0;
        buscarTemaABorrar(paginaCan);
    });

    //Botones Limpiadores de cargas:
    $("#limpiar_artistas").click(function (e) {
        e.preventDefault();
        $('#paginador_art').empty();
        $('#artistas_a_borrar').empty();
        ocultarBajaArtistas();
    });
    $("#limpiar_albumes").click(function (e) {
        e.preventDefault();
        $('#paginador_alb').empty();
        $('#albumes_a_borrar').empty();
        ocultarBajaAlbumes();
    });
    $("#limpiar_temas").click(function (e) {
        e.preventDefault();
        $('#paginador_t').empty();
        $('#temas_a_borrar').empty();
        ocultarBajaTemas();
    });
    $('#limpiar_usuarios').click(function (e) {
        e.preventDefault();
        $('#paginador_us').empty();
        $('#rep_usuarios').empty();
        $('#contenido_reporte_usuarios').hide();
    });
    $('#limpiar_reproducciones').click(function (e) {
        e.preventDefault();
        $('#paginador_rep').empty();
        $('#rep_reproducciones').empty();
        $('#contenido_reporte_reproducciones').hide();
    });

    //Cargar playlists de usuario
    cargarPlaylistsUsuario();

});
/*--MENU USUARIO--*/
function busquedaPrincipal(p, search) {
    var aBus = $('#aBuscar').val();
    if (search !== "")
        aBus = search;

    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded",
        timeout: 4000,
        url: "funcionalidadesUsuario.php",
        data: "accion=listarBusqueda&p=" + p + "&aBuscar=" + aBus
    }).done(function (datos) {
        $("#resultados_busqueda").empty();

        cantidadDePaginasBus = parseInt(datos.cantidad);
        paginaBus = parseInt(datos.pagina);

        var cabezal = $("<tr class='tituloPlaylist'><td colspan='10'>Resultados</td></tr><tr><th>Nombre</th><th>Play</th><th>Duracion</th><th>Album</th>\n\
        <th>Artista</th><th>Reproducciones</th><th>Rate</th><th>Global</th><th>Album Cover</th><th>Add</th></tr>");
        $("#resultados_busqueda").append(cabezal);

        $('#resultados_busqueda').show();
        //actualizo la busqueda:
        ultimaBusqueda = aBus;

        for (i = 0; i < datos.resultados.length; i++) {
            agregarResultado(datos.resultados[i], datos.valoraciones[i], datos.rating[i]);
        }
        if (datos.resultados.length == 0) {
            var noResults = $("<tr><td colspan='10'>No hay resultados de la busqueda</td></tr>");
            $("#resultados_busqueda").append(noResults);
            $("#paginador_res").empty();
        } else {
            agregarPaginado(paginaBus, cantidadDePaginasBus, "resultado");
        }
    }).fail(function (x, error) {

    });
}

function agregarResultado(res, val, rank) {
    var rating = "<td class='star-rating' width='200px' id='rate" + res.Id + "'><fieldset>\n\
    <input type='radio' id='star5" + res.Id + "' name='rating" + res.Id + "' value='5' onclick='updRating(" + res.Id + "," + 5 + ");'/><label for='star5" + res.Id + "'>5 stars</label>\n\
    <input type='radio' id='star4" + res.Id + "' name='rating" + res.Id + "' value='4' onclick='updRating(" + res.Id + "," + 4 + ");'/><label for='star4" + res.Id + "'>4 stars</label>\n\
    <input type='radio' id='star3" + res.Id + "' name='rating" + res.Id + "' value='3' onclick='updRating(" + res.Id + "," + 3 + ");'/><label for='star3" + res.Id + "'>3 stars</label>\n\
    <input type='radio' id='star2" + res.Id + "' name='rating" + res.Id + "' value='2' onclick='updRating(" + res.Id + "," + 2 + ");'/><label for='star2" + res.Id + "'>2 stars</label>\n\
    <input type='radio' id='star1" + res.Id + "' name='rating" + res.Id + "' value='1' onclick='updRating(" + res.Id + "," + 1 + ");'/><label for='star1" + res.Id + "'>1 stars</label></fieldset></td>";

    var global = "<td width='70'><img src='imgs/" + val + ".png' id='global" + res.Id + "'width='25px' style='margin-right:5px'><img src='imgs/star.png' width='25px'></td>";
    var cover = "<td><img src='uploads/" + res.Cover + "' width='100' height='100'></td>";
    var audio = "<td width='50px'><audio id='song" + res.Id + "'><source src='uploads/" + res.AudioSource + "' type='audio/mpeg'></audio><input type='image' src='imgs/play.png' width='35px' id='img" + res.Id + "' onclick='playPause(" + res.Id + ")'></input></td>";
    var agregar = "<td width='40px'><input type='image' src='imgs/add.png' width='30px' id='add" + res.Id + "' onclick='existeAlgunaPlayList(" + res.Id + ")'/></td>";

    var aInsertar = $("<tr><td>" + res.Cancion + "</td>" + audio + "<td>" + res.Duration + "</td><td>" + res.Album + "</td><td>" + res.Artista +
            "</td><td id='reps" + res.Id + "'>" + res.Reproducciones + "</td>" + rating + global + cover + agregar + "</tr>");
    $("#resultados_busqueda").append(aInsertar);

    //checkea estrellas segun rating de la BD por el usuario
    ratingAEstrellas(res.Id, rank);

}

function cargarPlaylistsUsuario() {
    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded",
        timeout: 4000,
        url: "funcionalidadesUsuario.php",
        data: "accion=cargarPlaylists"
    }).done(function (datos) {
        mailActual = datos.mailUsuario;
        $("#playlists_usuario").empty();
        if (datos.playlists.length == 0) {
            $("#resultados_playlist").hide();
            $("#c_paginador_pl").hide();
            $("#playlists_usuario").hide();
        } else {
            $("#playlists_usuario").show();
            for (i = 0; i < datos.playlists.length; i++) {
                var nom = datos.playlists[i];
                var a = $("<a />").attr({"href": "#", "onclick": "displayPlaylist('" + nom + "','" + paginaPL + "')"});
                a.text(nom);
                var del = $("<img />").attr({"src": "imgs/del.png", "width": "20px"});
                var linkBorr = $("<a/>").attr({"href": "#", "onclick": "borrarPlaylist('" + nom + "')", "style": "float: right;"});
                linkBorr.append(del);
                var l = $("<li id='" + nom + "'/>");
                l.append(a);
                l.append(linkBorr);
                $("#playlists_usuario").append(l);
            }
        }
    }).fail(function (x, error) {
    });
}

function borrarPlaylist(nombre) {
    console.log('se llama borrar a ' + nombre);
    alertify.confirm("Desea borrar la playlist: " + nombre + " ?", function (e, str) {
        if (e) {
            $.ajax({
                async: true,
                type: "POST",
                dataType: "json",
                contentType: "application/x-www-form-urlencoded",
                timeout: 4000,
                url: "funcionalidadesUsuario.php",
                data: "accion=borrarPlaylist&playlist=" + nombre
            }).done(function (datos) {
                //Si la playlist borrada es la actual, vacio las tablas
                if (nombre == playlistActual) {
                    $("#resultados_playlist").empty();
                    $("#c_paginador_pl").empty();
                }
                cargarPlaylistsUsuario();

                alertify.success("Playlist borrada");
            }).fail(function (x, error) {
                alertify.error("Error al borrar");
            });
        } else {
            alertify.error("Cancelar");
        }
    });
}

function displayPlaylist(nombre, p) {
    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded",
        timeout: 4000,
        url: "funcionalidadesUsuario.php",
        data: "accion=mostrarPlaylist&nombre=" + nombre + "&p=" + p
    }).done(function (datos) {
        $("#resultados_playlist").empty();
        $("#playlists_usuario").show();
        playlistActual = nombre;
        cantidadDePaginasPL = parseInt(datos.cantidad);
        paginaPL = parseInt(datos.pagina);

        var cabezal = $("<tr class='tituloPlaylist'><td colspan='10'>"+nombre+"</td></tr><tr><th>Nombre</th><th>Play</th><th>Duracion</th><th>Album</th>\n\
        <th>Artista</th><th>Reproducciones</th><th>Rate</th><th>Global</th><th>Album Cover</th><th>Del</th></tr>");

        $("#resultados_playlist").append(cabezal);
        $('#resultados_playlist').show();

        for (i = 0; i < datos.resultados.length; i++) {
            //Pasarle nombre de parametro
            agregarCancionPL(datos.resultados[i], datos.valoraciones[i], datos.rating[i]);
        }
        if (datos.resultados.length === 0) {
            var noResults = $("<tr><td colspan='10'>No hay resultados de la busqueda</td></tr>");
            $("#resultados_playlist").append(noResults);
            $("#paginador_pl").empty();
        } else {
            agregarPaginado(paginaPL, cantidadDePaginasPL, "playlist");
        }
    }).fail(function (x, error) {
    });
}

function agregarCancionPL(res, val, rank) {
    var idCPL = res.Id + 999;//para evitar colisiones
    var rating = "<td class='star-rating' width='200px' id='rate" + idCPL + "'><fieldset>\n\
    <input type='radio' id='star5" + idCPL + "' name='rating" + idCPL + "' value='5' onclick='updRatingPL(" + res.Id + "," + 5 + ");'/><label for='star5" + idCPL + "'>5 stars</label>\n\
    <input type='radio' id='star4" + idCPL + "' name='rating" + idCPL + "' value='4' onclick='updRatingPL(" + res.Id + "," + 4 + ");'/><label for='star4" + idCPL + "'>4 stars</label>\n\
    <input type='radio' id='star3" + idCPL + "' name='rating" + idCPL + "' value='3' onclick='updRatingPL(" + res.Id + "," + 3 + ");'/><label for='star3" + idCPL + "'>3 stars</label>\n\
    <input type='radio' id='star2" + idCPL + "' name='rating" + idCPL + "' value='2' onclick='updRatingPL(" + res.Id + "," + 2 + ");'/><label for='star2" + idCPL + "'>2 stars</label>\n\
    <input type='radio' id='star1" + idCPL + "' name='rating" + idCPL + "' value='1' onclick='updRatingPL(" + res.Id + "," + 1 + ");'/><label for='star1" + idCPL + "'>1 stars</label></fieldset></td>";

    var global = "<td width='70'><img src='imgs/" + val + ".png' id='global" + idCPL + "'width='25px' style='margin-right:5px'><img src='imgs/star.png' width='25px'></td>";
    var cover = "<td><img src='uploads/" + res.Cover + "' width='100' height='100'></td>";
    var audio = "<td width='50px'><audio id='song" + idCPL + "'><source src='uploads/" + res.AudioSource + "' type='audio/mpeg'></audio><input type='image' src='imgs/play.png' width='35px' id='img" + idCPL + "' onclick='playPausePL(" + res.Id + ")'></input></td>";
    var borrar = "<td width='40px'><input type='image' src='imgs/del.png' width='35px' id='add" + idCPL + "' onclick='sacarDePlaylist(" + res.Id + ")'/></td>";

    var aInsertar = $("<tr><td>" + res.Cancion + "</td>" + audio + "<td>" + res.Duration + "</td><td>" + res.Album + "</td><td width='110px'>" + res.Artista +
            "</td><td id='reps" + idCPL + "'>" + res.Reproducciones + "</td>" + rating + global + cover + borrar + "</tr>");
    $("#resultados_playlist").append(aInsertar);

    //checkea estrellas segun rating de la BD por el usuario
    ratingAEstrellas(idCPL, rank);
}
//Borra una cancion seleccionada de la actual playlist
function sacarDePlaylist(idC) {
    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded",
        timeout: 4000,
        url: "funcionalidadesUsuario.php",
        data: "accion=borrarDePlaylist&tema=" + idC + "&playlist=" + playlistActual
    }).done(function (datos) {
        displayPlaylist(playlistActual, paginaPL);
        alertify.success("Borrada correctamente");
    }).fail(function (x, error) {
    });

}

function existeAlgunaPlayList(idC) {
    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded",
        timeout: 4000,
        url: "funcionalidadesUsuario.php",
        data: "accion=existeAlgunaPS"
    }).done(function (datos) {
        if (datos == "ok") {
            agregarAPlaylist(idC);
        } else {
            crearPlaylistYAgregar(idC);
        }
    }).fail(function (x, error) {
    });
}

function crearPlaylistYAgregar(idC) {
    var pls = [];
    $("#playlists_usuario li").each(function () {
        pls.push($(this).attr('id'));
    });
    alertify.prompt("Nueva Playlist", function (e, str) {
        if (e) {
            console.log(str);
            console.log(pls[0]);
            //me fijo si es unica
            var unica = true;
            for (var i = 0; i < pls.length; i++) {
                if (pls[i] == str) {
                    unica = false;
                }
            }
            if (str != "" && unica) {
                cargarCancionAPlaylist(idC, str);
                alertify.success("Playlist creada correctamente");
                cargarPlaylistsUsuario();
            } else {
                alertify.error("Playlist: Debe ser unica y no ingreso nada");
            }
        } else {
            alertify.error("Cancelar");
        }
    }, "");
}

function agregarAPlaylist(idC) {
    var pls = [];
    $("#playlists_usuario li").each(function () {
        pls.push($(this).attr('id'));
    });
    alertify.set({labels: {ok: "Si", cancel: "No"}});
    alertify.confirm("Desea agregarla a un playlist existente?", function (e, str) {
        alertify.set({labels: {ok: "Ok", cancel: "Cancel"}});
        //Confirma
        if (e) {
            alertify.prompt("Escriba el nombre de la Playlist", function (e, str) {
                if (e) {
                    var unica = true;
                    for (var i = 0; i < pls.length; i++) {
                        if (pls[i] == str) {
                            unica = false;
                        }
                    }
                    if (!unica) {
                        cargarCancionAPlaylist(idC, str);
                    } else {
                        alertify.error("Playlist: No existe dicha playlist");
                    }
                } else {
                    alertify.error("Cancelar");
                }
            }, "");

        } else {
            alertify.prompt("Nueva Playlist", function (e, str) {
                if (e) {
                    //me fijo si es unica
                    var unica = true;
                    for (var i = 0; i < pls.length; i++) {
                        if (pls[i] == str) {
                            unica = false;
                        }
                    }
                    if (str != "" && unica) {
                        cargarCancionAPlaylist(idC, str);
                        alertify.success("Playlist creada correctamente");
                        cargarPlaylistsUsuario();
                    } else {
                        alertify.error("Playlist: Debe ser unica y no ingreso nada");
                    }
                } else {
                    alertify.error("Cancelar");
                }
            }, "");
        }
    });
}

function cargarCancionAPlaylist(idC, pl) {
    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded",
        timeout: 4000,
        url: "funcionalidadesUsuario.php",
        data: "accion=cargarCancionAPL&idTema=" + idC + "&playlist=" + pl
    }).done(function (datos) {
        if (datos == "ok") {
            if (pl == playlistActual) {
                displayPlaylist(playlistActual, paginaPL);
            }
            alertify.success("Cancion agregada correctamente");
        } else if (datos == "noUnica") {
            alertify.error("Ya esta en la playlist");
        }
    }).fail(function (x, error) {
        alertify.error("Error al agregar");
    });
}

//recibe un id de cancion, un rating. Calcula el rating global y lo actualiza en la columna Global.  
function updRating(idC, rate) {
    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded",
        timeout: 4000,
        url: "funcionalidadesUsuario.php",
        data: "accion=actualizarRating&idTema=" + idC + "&rating=" + rate
    }).done(function (datos) {
        $("#global" + idC).attr("src", "imgs/" + datos + ".png");
        console.log(datos);
    }).fail(function (x, error) {
    });
}

function updRatingPL(idC, rate) {
    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded",
        timeout: 4000,
        url: "funcionalidadesUsuario.php",
        data: "accion=actualizarRating&idTema=" + idC + "&rating=" + rate
    }).done(function (datos) {
        var g = idC + "" + 999;
        console.log(g + " con global " + datos);
        $("#global" + g).attr("src", "imgs/" + datos + ".png");

    }).fail(function (x, error) {
    });
}

function ratingAEstrellas(idC, rate) {
    //Asegurarme que rate es numero. Hacerle ceil() antes
    switch (rate) {
        case 1:
            $("#star1" + idC).prop("checked", true);
            break;
        case 2:
            $("#star1" + idC).prop("checked", true);
            $("#star2" + idC).prop("checked", true);
            break;
        case 3:
            $("#star1" + idC).prop("checked", true);
            $("#star2" + idC).prop("checked", true);
            $("#star3" + idC).prop("checked", true);
            break;
        case 4:
            $("#star1" + idC).prop("checked", true);
            $("#star2" + idC).prop("checked", true);
            $("#star3" + idC).prop("checked", true);
            $("#star4" + idC).prop("checked", true);
            break;
        case 5:
            $("#star1" + idC).prop("checked", true);
            $("#star2" + idC).prop("checked", true);
            $("#star3" + idC).prop("checked", true);
            $("#star4" + idC).prop("checked", true);
            $("#star5" + idC).prop("checked", true);
            break;
        default:
    }
}

function playPause(idTem) {
    console.log("id tema = " + idTem);
    var audio = $("#song" + idTem + "")[0];
    var imag = $("#img" + idTem + "");
    if (audio.paused) {
        imag.attr("src", "imgs/pause.png");
        audio.play();

        if (idTem !== temaActual) {
            temaActual = idTem;
            $.ajax({
                async: true,
                type: "POST",
                dataType: "json",
                contentType: "application/x-www-form-urlencoded",
                timeout: 4000,
                url: "funcionalidadesUsuario.php",
                data: "accion=actualizarReproducciones&idCancion=" + idTem
            }).done(function (datos) {
                //Actualizo columa de reproducciones para ese tema
                $("#reps" + idTem).text(datos);
            }).fail(function (x, error) {
                //No actualizo por alguna razon
            });
        }
    } else {
        audio.pause();
        imag.attr("src", "imgs/play.png");
    }
}

function playPausePL(idTem) {
    console.log("id tema = " + idTem);
    var g = idTem + "" + 999;
    var audio = $("#song" + g + "")[0];
    var imag = $("#img" + g + "");
    if (audio.paused) {
        imag.attr("src", "imgs/pause.png");
        audio.play();

        if (idTem !== temaActual) {
            temaActual = idTem;
            $.ajax({
                async: true,
                type: "POST",
                dataType: "json",
                contentType: "application/x-www-form-urlencoded",
                timeout: 4000,
                url: "funcionalidadesUsuario.php",
                data: "accion=actualizarReproducciones&idCancion=" + idTem
            }).done(function (datos) {
                //Actualizo columa de reproducciones para ese tema
                $("#reps" + g).text(datos);
            }).fail(function (x, error) {
                //No actualizo por alguna razon
            });
        }
    } else {
        audio.pause();
        imag.attr("src", "imgs/play.png");
    }
}

/*-- MENU ADMINISTRADOR --*/
/*OCULTAR MENUES*/
function ocultarBajaArtistas() {
    $('#artistas_a_borrar').hide();
    $('#paginador_art').hide();
    $('#c_paginador_art').hide();
}

function ocultarBajaAlbumes() {
    $('#albumes_a_borrar').hide();
    $('#paginador_alb').hide();
    $('#c_paginador_alb').hide();
}

function ocultarBajaTemas() {
    $("#temas_a_borrar").hide();
    $("#paginador_t").hide();
    $("#c_paginador_t").hide();
}

/*REPORTES*/
function cargarReporteReproducciones(p) {
    $("#rep_reproducciones").html('<img src="imgs/cargando.gif" height="40" width="40"/>');
    $('#contenido_reporte_reproducciones').show();

    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded",
        timeout: 4000,
        url: "busquedasAdmin.php",
        data: "accion=listarReproducciones&p=" + p
    }).done(function (datos) {
        $("#rep_reproducciones").empty();
        cantidadDePaginasRep = parseInt(datos.cantidad);
        paginaRep = parseInt(datos.pagina);

        var cabezal = $("<tr><th>Artista</th><th>Pais de origen</th><th>Reproducciones</th></tr>");
        $("#rep_reproducciones").append(cabezal);

        for (i = 0; i < datos.reproducciones.length; i++) {
            agregarReproduccion(datos.reproducciones[i]);
        }
        //Agregar aviso no hay resultados si datos.artistas.length == 0

        agregarPaginado(paginaRep, cantidadDePaginasRep, "reproduccion");

    }).fail(function (x, error) {
        $("#rep_reproducciones").html('<li>Error: ' + error + '</li>');
    });
}

function agregarReproduccion(rep) {
    var aInsertar = $("<tr><td>" + rep.Nombre + "</td><td>" + rep.Pais +
            "</td><td>" + rep.Reproducciones + "</td></tr>");
    $("#rep_reproducciones").append(aInsertar);
}

function cargarReporteUsuarios(p) {
    $("#rep_usuarios").html('<img src="imgs/cargando.gif" height="40" width="40"/>');
    $('#contenido_reporte_usuarios').show();

    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded",
        timeout: 4000,
        url: "busquedasAdmin.php",
        data: "accion=listarUsuarios&p=" + p
    }).done(function (datos) {
        $("#rep_usuarios").empty();
        cantidadDePaginasUs = parseInt(datos.cantidad);
        paginaUs = parseInt(datos.pagina);

        var cabezal = $("<tr><th>Nombre</th><th>Mail</th></tr>");
        $("#rep_usuarios").append(cabezal);

        for (i = 0; i < datos.usuarios.length; i++) {
            agregarUsuario(datos.usuarios[i]);
        }

        agregarPaginado(paginaUs, cantidadDePaginasUs, "usuario");

    }).fail(function (x, error) {
        $("#rep_usuarios").html('<li>Error: ' + error + '</li>');
    });
}

function agregarUsuario(us) {
    var aInsertar = $("<tr><td>" + us.Nombre + "</td><td>" + us.Mail + "</td></tr>");
    $("#rep_usuarios").append(aInsertar);
}

/*BUSQUEDA ARTISTAS*/
function buscarArtistaABorrar(p) {
    var v = $("#busqueda_art").val();
    $("#artistas_a_borrar").html('<img src="imgs/cargando.gif" height="40" width="40"/>');
    $("#artistas_a_borrar").show();

    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded",
        timeout: 4000,
        url: "busquedasAdmin.php",
        data: "accion=listarArtistas&artista=" + v + "&p=" + p
    }).done(function (datos) {
        $("#artistas_a_borrar").empty();
        cantidadDePaginasArt = parseInt(datos.cantidad);
        paginaArt = parseInt(datos.pagina);

        if (datos.artistas.length === 0) {
            $('#artistas_a_borrar').html('<li>No se encontraron resultados a su busqueda</li>');
        } else {
            for (i = 0; i < datos.artistas.length; i++) {
                agregarArtista(datos.artistas[i]);
            }
            agregarPaginado(paginaArt, cantidadDePaginasArt, "artista");
        }

        $("#paginador_art").show();
        $("#c_paginador_art").show();
    }).fail(function (x, error) {
        $("#artistas_a_borrar").html('<li>Error: ' + error + '</li>');
    });
}

function agregarArtista(art) {
    var li = $("<li />");
    var h4 = $("<h4 />").text("[" + art.Nombre + "]  -  " + art.Pais);
    h4.addClass("colorBlanco");
    var a = $("<a />").attr({"href": "#" + art.Id, "onclick": "borrarArtista(" + art.Id + ")"});
    a.addClass("borrar");
    a.append($("<img />").attr("src", "imgs/eliminar.png"));

    h4.append(a);
    li.append(h4);
    $("#artistas_a_borrar").append(li);
}

function borrarArtista(id) {
    console.log('Se llama a borrar con id = ' + id);
    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded",
        timeout: 4000,
        url: "busquedasAdmin.php",
        data: "accion=borrarArtista&artistaABorrar=" + id
    }).done(function (datos) {
        alert("borrado correctamente");
        buscarArtistaABorrar(paginaArt);
    }).fail(function (x, error) {
        alert("error al borrar");
    });
}

/*BUSQUEDA ALBUMES*/
function buscarAlbumABorrar(p) {
    var v = $("#busqueda_alb").val();
    $("#albumes_a_borrar").html('<img src="imgs/cargando.gif" height="40" width="40"/>');
    $("#albumes_a_borrar").show();
    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded",
        timeout: 4000,
        url: "busquedasAdmin.php",
        data: "accion=listarAlbumes&album=" + v + "&p=" + p
    }).done(function (datos) {
        $("#albumes_a_borrar").empty();
        cantidadDePaginasAlb = parseInt(datos.cantidad);
        paginaAlb = parseInt(datos.pagina);

        if (datos.albumes.length === 0) {
            $("#albumes_a_borrar").html('<li>No se encontraron resultados a su busqueda</li>');
        } else {
            for (i = 0; i < datos.albumes.length; i++) {
                agregarAlbum(datos.albumes[i], datos.nombreArtistas[i]);
            }
            agregarPaginado(paginaAlb, cantidadDePaginasAlb, "album");
        }

        $("#paginador_alb").show();
        $("#c_paginador_alb").show();
    }).fail(function (x, error) {
        $("#albumes_a_borrar").html('<li>Error: ' + error + '</li>');
    });
}

function agregarAlbum(alb, nomArt) {
    var li = $("<li />");
    var h4 = $("<h4 />").text("[" + alb.Nombre + "]  -  " + nomArt);
    h4.addClass("colorBlanco");
    var a = $("<a />").attr({"href": "#" + alb.Id, "onclick": "borrarAlbum(" + alb.Id + ")"});

    a.addClass("borrar");
    a.append($("<img />").attr("src", "imgs/eliminar.png"));
    h4.append(a);
    li.append(h4);
    $("#albumes_a_borrar").append(li);
}

function borrarAlbum(id) {
    console.log("Se llama a borrar album con id = " + id);
    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded",
        timeout: 4000,
        url: "busquedasAdmin.php",
        data: "accion=borrarAlbum&albumABorrar=" + id
    }).done(function (datos) {
        alert("borrado correctamente");
        buscarAlbumABorrar(paginaAlb);
    }).fail(function (x, error) {
        alert("error al borrar");
    });
}

/*BUSQUEDA TEMAS*/
function buscarTemaABorrar(p) {
    var v = $("#a_buscar").val();
    $("#temas_a_borrar").html('<img src="imgs/cargando.gif" height="40" width="40"/>');
    $("#temas_a_borrar").show();
    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded",
        timeout: 4000,
        url: "busquedasAdmin.php",
        data: "accion=listarTemas&cancion=" + v + "&p=" + p
    }).done(function (datos) {
        $("#temas_a_borrar").empty();
        cantidadDePaginasCan = parseInt(datos.cantidad);
        paginaCan = parseInt(datos.pagina);

        if (datos.canciones.length === 0) {
            $("#temas_a_borrar").html('<li>No se encontraron resultados a su busqueda</li>');
        } else {
            for (i = 0; i < datos.canciones.length; i++) {
                agregarCancion(datos.canciones[i], datos.nombreAlbumes[i], datos.nombreArtistas[i]);
            }
            agregarPaginado(paginaCan, cantidadDePaginasCan, "tema");
        }

        $("#c_paginador_t").show();
        $("#paginador_t").show();
    }).fail(function (x, error) {
        $("#temas_a_borrar").html('<li>Error: ' + error + '</li>');
    });
}

function agregarCancion(canc, alb, art) {
    var li = $("<li />");
    var h4 = $("<h4 />").text("[" + canc.Nombre + "]  -  " + alb + "  -  " + art);
    h4.addClass("colorBlanco");
    var a = $("<a />").attr({"href": "#" + canc.Id, "onclick": "borrarCancion(" + canc.Id + ")"});

    a.addClass("borrar");
    a.append($("<img />").attr("src", "imgs/eliminar.png"));
    h4.append(a);
    li.append(h4);
    $("#temas_a_borrar").append(li);
}

function borrarCancion(id) {
    console.log("se llama a borrar cancion con id = " + id);
    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded",
        timeout: 4000,
        url: "busquedasAdmin.php",
        data: "accion=borrarTema&temaABorrar=" + id
    }).done(function (datos) {
        alert("borrado correctamente");
        buscarTemaABorrar(paginaCan);
    }).fail(function (x, error) {
        alert("error al borrar");
    });
}

/*CARGAR SELECTORES*/
function cargarMinutosYSegundos() {
    $('select[name=minutos]').append($("<option />").val('mm').text('mm'));
    $('select[name=segundos]').append($("<option />").val('ss').text('ss'));
    $i = 0;
    for ($i; $i < 60; $i++) {
        $num = ($i < 10) ? "0" + $i : $i;
        $('select[name=minutos]').append($("<option />").val($num).text($num));
        $('select[name=segundos]').append($("<option />").val($num).text($num));
    }
}

/*PAGINADOS Y NAVEGABILIDAD*/
function agregarPaginado(pag, cantPags, tipo) {
    if (tipo === "tema") {
        var html = $("#paginador_t").empty();
    } else if (tipo === "album") {
        var html = $("#paginador_alb").empty();
    } else if (tipo === "artista") {
        var html = $("#paginador_art").empty();
    } else if (tipo === "usuario") {
        var html = $("#paginador_us").empty();
    } else if (tipo === "reproduccion") {
        var html = $("#paginador_rep").empty();
    } else if (tipo === "resultado") {
        var html = $("#paginador_res").empty();
    } else if (tipo === "playlist") {
        var html = $("#paginador_pl").empty();
    }

    var anterior = pag - 1 < 0 ? 0 : pag - 1;
    var siguiente = pag + 1 > cantPags - 1 ? cantPags - 1 : pag + 1;

    html.append($("<a />").append("<img src='imgs/first.png' height='15px' width='15px'>").attr("href", "#0"));
    html.append(" ");
    html.append($("<a />").append("<img src='imgs/previous.png' height='15px' width='15px'>").attr("href", "#" + anterior));
    html.append(" ");

    for (var i = 0; i < cantPags; i++) {
        var a = $("<a />").text((i + 1)).attr("href", "#" + i);
        a.addClass("numeroPaginador");

        if (i === pag)
            a.addClass("seleccionado");

        html.append(a);
        html.append(" ");
    }
    html.append($("<a />").append("<img src='imgs/next.png' height='15px' width='15px'>").attr("href", "#" + siguiente));
    html.append(" ");
    html.append($("<a />").append("<img src='imgs/last.png' height='15px' width='15px'>").attr("href", "#" + (cantPags - 1)));

    if (tipo === "tema") {
        $("#paginador_t a").click(function (e) {
            e.preventDefault();
            var p = $(this).attr("href").replace("#", "");
            buscarTemaABorrar(p);
        });
    } else if (tipo === "album") {
        $("#paginador_alb a").click(function (e) {
            e.preventDefault();
            var p = $(this).attr("href").replace("#", "");
            buscarAlbumABorrar(p);
        });
    } else if (tipo === "artista") {
        $("#paginador_art a").click(function (e) {
            e.preventDefault();
            var p = $(this).attr("href").replace("#", "");
            buscarArtistaABorrar(p);
        });
    } else if (tipo === "usuario") {
        $("#paginador_us a").click(function (e) {
            e.preventDefault();
            var p = $(this).attr("href").replace("#", "");
            cargarReporteUsuarios(p);
        });
    } else if (tipo === "reproduccion") {
        $("#paginador_rep a").click(function (e) {
            e.preventDefault();
            var p = $(this).attr("href").replace("#", "");
            cargarReporteReproducciones(p);
        });
    } else if (tipo === "resultado") {
        $("#paginador_res a").click(function (e) {
            e.preventDefault();
            var p = $(this).attr("href").replace("#", "");
            busquedaPrincipal(p, ultimaBusqueda);
        });
    } else if (tipo === "playlist") {
        $("#paginador_pl a").click(function (e) {
            e.preventDefault();
            var p = $(this).attr("href").replace("#", "");
            busquedaPrincipal(p, ultimaBusqueda);
        });
    }
}


