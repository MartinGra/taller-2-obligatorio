<?php

include_once("config_BD.php");
session_start();

$accion = strlen(filter_input(INPUT_POST, 'accion')) ? filter_input(INPUT_POST, 'accion') : filter_input(INPUT_GET, 'accion');
$cantElementosPorPagina = 10;
$resultado = "";

if ($accion == "listarBusqueda") {
    $conn->conectar();

    $cantidad = 0;
    $inp = strlen(filter_input(INPUT_POST, 'aBuscar')) ? filter_input(INPUT_POST, 'aBuscar') : '';
    $p = strlen(filter_input(INPUT_POST, 'p')) ? filter_input(INPUT_POST, 'p') : 0;

    $obtenerCant = "select count(*) as cantidad from (select t.Nombre as Cancion, al.Nombre as Album, ar.Nombre as Artista, t.*
            from Canciones t, Albumes al, Artistas ar where t.AlbumId = al.Id and al.IdArtista=ar.id) k
            where lower(Cancion) like lower('%$inp%') or lower(Album) like lower('%$inp%') or lower(Artista) like lower('%$inp%')";

    if ($conn->consulta($obtenerCant)) {
        $result = $conn->siguienteRegistro();
        $cantidad = $result['cantidad'];
    }

    $obtenerRes = "select * from (select t.Nombre as Cancion, al.Nombre as Album, ar.Nombre as Artista,al.ImagenSource as Cover, t.*
            from Canciones t, Albumes al, Artistas ar where t.AlbumId = al.Id and al.IdArtista=ar.id order by t.Nombre asc) k
            where lower(Cancion) like lower('%$inp%') or lower(Album) like lower('%$inp%') or lower(Artista) like lower('%$inp%')
            limit :offset, :cantidad";

    $parametros = array();
    $parametros[] = array("offset", $p * $cantElementosPorPagina, "int");
    $parametros[] = array("cantidad", $cantElementosPorPagina, "int");

    if ($conn->consulta($obtenerRes, $parametros)) {
        $result = $conn->restantesRegistros();
        $valoraciones = array();
        $ratings = array();
        $mail = $_SESSION['user']['mail'];
        foreach ($result as $resTemp) {
            $idTemaTemp = $resTemp['Id'];
            $valoraciones[] = obtenerPromedioValoracion($idTemaTemp, $conn);
            $ratings[] = obtenerValoracionDelUsuario($idTemaTemp, $mail, $conn);
        }
        $arr = array(
            "cantidad" => ceil($cantidad / $cantElementosPorPagina),
            "pagina" => $p,
            "resultados" => $result,
            "valoraciones" => $valoraciones,
            "rating" => $ratings
        );
        $resultado = json_encode($arr);
    } else {
        $resultado = $conn->ultimoError();
    }

    $conn->desconectar();
} else if ($accion == "actualizarReproducciones") {
    $temId = strlen(filter_input(INPUT_POST, 'idCancion')) ? filter_input(INPUT_POST, 'idCancion') : '';
    $conn->conectar();

    $ret = actualizarReps($temId, $conn);
    $resultado = ($ret) ? json_encode($ret) : "fail";

    $conn->desconectar();
} else if ($accion == "actualizarRating") {
    $tem = strlen(filter_input(INPUT_POST, 'idTema')) ? filter_input(INPUT_POST, 'idTema') : '';
    $rate = strlen(filter_input(INPUT_POST, 'rating')) ? filter_input(INPUT_POST, 'rating') : '';
    $mailUsuario = $_SESSION['user']['mail'];

    $conn->conectar();

    $res = actualizarRating($tem, $rate, $mailUsuario, $conn);
    $resultado = ($res) ? json_encode($res) : "fail";

    $conn->desconectar();
} else if ($accion == "cargarPlaylists") {
    $mail = $_SESSION['user']['mail'];
    $conn->conectar();
    $sql = "select distinct Nombre from Playlists where MailUsuario='$mail'";
    if ($conn->consulta($sql)) {
        $result = $conn->restantesRegistros();

        $playlists = array();

        foreach ($result as $temp) {
            $playlists[] = $temp['Nombre'];
        }
        $arr = array(
            "playlists" => $playlists,
            "mailUsuario" => $mail
        );
        $resultado = json_encode($arr);
    } else {
        $resultado = "fail al cargar playlists";
    }
    $conn->desconectar();
} else if ($accion == "mostrarPlaylist") {
    $mailUsuario = $_SESSION['user']['mail'];
    $nombre = strlen(filter_input(INPUT_POST, 'nombre')) ? filter_input(INPUT_POST, 'nombre') : '';
    $p = strlen(filter_input(INPUT_POST, 'p')) ? filter_input(INPUT_POST, 'p') : 0;
    $cantidad = 0;
    $conn->conectar();

    $tam = "select count(*) as cantidad from Playlists where MailUsuario='$mailUsuario' and Nombre='$nombre'";
    if ($conn->consulta($tam)) {
        $result = $conn->siguienteRegistro();
        $cantidad = $result['cantidad'];
    }
    //obtengo datos de resultados con canciones de la playlist

    $obtenerRes = "select * from (select t.Id as IdSong,t.Nombre as Cancion, al.Nombre as Album, ar.Nombre as Artista,al.ImagenSource as Cover, t.*
                from Canciones t, Albumes al, Artistas ar where t.AlbumId = al.Id and al.IdArtista=ar.id order by t.Nombre asc) k ,
                (select IdCancion from Playlists where MailUsuario='$mailUsuario' and Nombre='$nombre') s where s.IdCancion=k.IdSong limit :offset, :cantidad";

    $parametros = array();
    $parametros[] = array("offset", $p * $cantElementosPorPagina, "int");
    $parametros[] = array("cantidad", $cantElementosPorPagina, "int");
    if ($conn->consulta($obtenerRes, $parametros)) {
        $result = $conn->restantesRegistros();
        $valoraciones = array();
        $ratings = array();
        foreach ($result as $resTemp) {
            $idTemaTemp = $resTemp['Id'];
            $valoraciones[] = obtenerPromedioValoracion($idTemaTemp, $conn);
            $ratings[] = obtenerValoracionDelUsuario($idTemaTemp, $mailUsuario, $conn);
        }
        $arr = array(
            "cantidad" => ceil($cantidad / $cantElementosPorPagina),
            "pagina" => $p,
            "resultados" => $result,
            "valoraciones" => $valoraciones,
            "rating" => $ratings
        );
        $resultado = json_encode($arr);
    } else {
        $resultado = $conn->ultimoError();
    }
    $conn->desconectar();
} else if ($accion == "existeAlgunaPS") {
    $conn->conectar();
    $mailUsuario = $_SESSION['user']['mail'];
    $sql = "select count(*) as cantidad from Playlists where MailUsuario='$mailUsuario'";
    if ($conn->consulta($sql)) {
        $result = $conn->siguienteRegistro();
        $resultado = ($result['cantidad'] > 0) ? json_encode("ok") : json_encode("no");
    } else {
        $resultado = $conn->ultimoError();
    }
    $conn->desconectar();
} else if ($accion == "cargarCancionAPL") {
    $mailUsuario = $_SESSION['user']['mail'];
    $idCancion = strlen(filter_input(INPUT_POST, 'idTema')) ? filter_input(INPUT_POST, 'idTema') : '';
    $nomPlaylist = strlen(filter_input(INPUT_POST, 'playlist')) ? filter_input(INPUT_POST, 'playlist') : '';

    $conn->conectar();
    $checkUnica = "select count(*) as cantidad from Playlists where MailUsuario='$mailUsuario' and Nombre='$nomPlaylist' and IdCancion='$idCancion'";

    if ($conn->consulta($checkUnica)) {
        $res = $conn->siguienteRegistro();
        if ($res['cantidad'] == 0) {
            $sql = "insert into Playlists (MailUsuario,Nombre,IdCancion) values ('$mailUsuario','$nomPlaylist','$idCancion')";
            if ($conn->consulta($sql)) {
                $resultado = json_encode("ok");
            } else {
                $resultado = $conn->ultimoError();
            }
        }else{
            $resultado = json_encode("noUnica");
        }
    }

    $conn->desconectar();
} else if ($accion == "borrarDePlaylist") {
    $idCancion = strlen(filter_input(INPUT_POST, 'tema')) ? filter_input(INPUT_POST, 'tema') : '';
    $mailUsuario = $_SESSION['user']['mail'];
    $nomPlaylist = strlen(filter_input(INPUT_POST, 'playlist')) ? filter_input(INPUT_POST, 'playlist') : '';
    $conn->conectar();

    $sql = "delete from Playlists where MailUsuario='$mailUsuario' and Nombre='$nomPlaylist' and IdCancion='$idCancion'";
    if ($conn->consulta($sql)) {
        $resultado = json_encode("ok");
    } else {
        $resultado = "error en la consulta";
    }

    $conn->desconectar();
}else if($accion == "borrarPlaylist"){
    $mailUsuario = $_SESSION['user']['mail'];
    $nomPlaylist = strlen(filter_input(INPUT_POST, 'playlist')) ? filter_input(INPUT_POST, 'playlist') : '';
    $conn->conectar();
    $sql = "delete from Playlists where MailUsuario='$mailUsuario' and Nombre='$nomPlaylist'";
    if($conn->consulta($sql)){
        $resultado = json_encode("ok");
    }else{
        $resultado = "error en la consulta";
    }
    $conn->desconectar();
}

function actualizarRating($id, $rating, $mail, $c) {
    $existencia = "SELECT count(*) as Cantidad FROM Valoraciones WHERE CancionId='$id' AND MailUsuario='$mail'";
    $ret = 0;

    if ($c->consulta($existencia)) {
        $result = $c->siguienteRegistro();
        //Existe valoracion
        if ($result['Cantidad'] > 0) {
            $actualizacion = "update Valoraciones set Rating='$rating' where CancionId='$id' and MailUsuario='$mail'";
            if ($c->consulta($actualizacion)) {
                //Una vez ingresada la valoracion al la BD devuelvo el rating global
                $ret = obtenerPromedioValoracion($id, $c);
            }
        } else {//No existe previamente     
            $insercion = "insert into Valoraciones (CancionId,MailUsuario,Rating) values ('$id','$mail','$rating')";
            if ($c->consulta($insercion)) {
                $ret = obtenerPromedioValoracion($id, $c); //seguramente convenga devolver 0 y hacer fail a $.ajax
            }
        }
    }
    return $ret;
}

function actualizarReps($id, $c) {
    $sql = "update Canciones
            set Reproducciones=Reproducciones+1
            where Id=$id";
    if ($c->consulta($sql)) {
        $obt = "select Reproducciones from Canciones where Id=$id";
        if ($c->consulta($obt)) {
            return $c->siguienteRegistro()['Reproducciones'];
        }
    } else {
        return 0;
    }
}

function obtenerValoracionDelUsuario($id, $mail, $c) {
    $ret = 0;
    $sql = "select Count(*) as Cantidad, COALESCE(Rating,0) as Rate from Valoraciones where CancionId='$id' and MailUsuario='$mail'";
    if ($c->consulta($sql)) {
        $result = $c->siguienteRegistro();
        if ($result['Cantidad'] > 0) {
            $ret = ceil($result['Rate']);
        }
    }
    return $ret;
}

function obtenerPromedioValoracion($id, $c) {
    $ret = 0;
    $sql = "select Count(*) as Cantidad, COALESCE(sum(Rating),0) as Suma from Valoraciones where CancionId=$id";
    if ($c->consulta($sql)) {
        $result = $c->siguienteRegistro();
        if ($result['Cantidad'] > 0) {
            $ret = ceil($result['Suma'] / $result['Cantidad']);
        }
    }
    return $ret;
}

echo $resultado;
